import 'package:flutter/material.dart';
import 'package:unsplash_tes/presenter/first_screen/FirstScreen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(useMaterial3: true), home: FirstScreen());
  }
}
