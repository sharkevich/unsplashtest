import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:unsplash_tes/domain/entities/PhotoCardModel.dart';
import 'package:unsplash_tes/model/Repository.dart';
import 'package:unsplash_tes/model/models/ErrorModel.dart';
import 'package:unsplash_tes/model/models/PhotoModel.dart';
import 'package:unsplash_tes/presenter/second_screen/SecondScreen.dart';

class FirstScreenBLoC {
  final Repository repo;
  final StreamController<List<PhotoCardModel>> dataStream = StreamController();
  List<PhotoModel> _photoModels = [];

  FirstScreenBLoC({required this.repo}) {
    getPhotos();
  }

  void onCardTap(BuildContext context, String? id) {
    final photoModel = _photoModels.firstWhere((element) => element.id == id);
    Navigator.of(context).push(CupertinoPageRoute(
        builder: (context) => SecondScreen(imageUrl: photoModel.urls?.full)));
  }

  void getPhotos() {
    repo.getListPhotos().then((photoModels) {
      _photoModels = List.from(photoModels);
      dataStream.add(photoModels
          .map<PhotoCardModel>((e) => e.toPhotoCardModel())
          .toList());
    }).onError((error, stackTrace) {
      dataStream.addError(error ?? ErrorModel());
    });
  }
}
