import 'package:flutter/material.dart';
import 'package:unsplash_tes/domain/entities/PhotoCardModel.dart';
import 'package:unsplash_tes/model/Repository.dart';
import 'package:unsplash_tes/model/models/ErrorModel.dart';
import 'package:unsplash_tes/presenter/first_screen/FirstScreenBLoC.dart';
import 'package:unsplash_tes/presenter/widgets/CustomCardWidget.dart';

class FirstScreen extends StatelessWidget {
  final _bloc = FirstScreenBLoC(repo: Repository());
  static const double padding = 16;

  FirstScreen({super.key});

  Widget _errorWidget(List<String> errorText) {
    return Center(
        child: Column(children: [
      ...errorText.map((e) => Text(e)).toList(),
      MaterialButton(
          onPressed: () => _bloc.getPhotos(), child: const Text("Update"))
    ]));
  }

  Widget _photoCardsWidget(List<PhotoCardModel> photoCardModels) {
    return SingleChildScrollView(
        child: Padding(
            padding: const EdgeInsets.all(padding),
            child: Column(
              children: photoCardModels.map((e) => _cardWidget(e)).toList(),
            )));
  }

  Widget _cardWidget(PhotoCardModel photoCardModel) {
    return Builder(
      builder: (context) {
        return GestureDetector(
          onTap: () => _bloc.onCardTap(context, photoCardModel.id),
          child: CustomCardWidget(photoCardModel: photoCardModel),
        );
      }
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text("Unsplash test project")),
        body: SafeArea(
            child: StreamBuilder(
                stream: _bloc.dataStream.stream,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    final photoCardModels = snapshot.data ?? [];
                    return _photoCardsWidget(photoCardModels);
                  } else if (snapshot.hasError) {
                    final errorText = (snapshot.error as ErrorModel).errors;
                    return _errorWidget(errorText ?? []);
                  } else {
                    return const Center(child: CircularProgressIndicator());
                  }
                })));
  }
}
