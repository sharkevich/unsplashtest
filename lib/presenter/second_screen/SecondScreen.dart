import 'package:flutter/material.dart';
import 'package:unsplash_tes/presenter/widgets/ImageWidget.dart';

class SecondScreen extends StatelessWidget {
  final String? imageUrl;

  const SecondScreen({super.key, this.imageUrl});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
            child: Center(child: ImageWidget(imageUrl: imageUrl ?? ""))));
  }
}
