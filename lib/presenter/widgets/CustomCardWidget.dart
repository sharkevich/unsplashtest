import 'package:flutter/material.dart';
import 'package:unsplash_tes/domain/entities/PhotoCardModel.dart';
import 'package:unsplash_tes/presenter/Constants.dart';
import 'package:unsplash_tes/presenter/widgets/ImageWidget.dart';

class CustomCardWidget extends StatelessWidget {
  static const double borderRadius = 12;
  final PhotoCardModel photoCardModel;

  const CustomCardWidget({super.key, required this.photoCardModel});

  @override
  Widget build(BuildContext context) {
    Widget namePhotoWidget = Padding(
        padding: const EdgeInsets.all(Constants.padding / 2),
        child: Text(photoCardModel.namePhoto ?? "",
            style: Theme.of(context).textTheme.titleMedium));

    Widget authorWidget = Padding(
        padding: const EdgeInsets.all(Constants.padding / 2),
        child: Text("Author: ${photoCardModel.authorName ?? ""}",
            style: Theme.of(context).textTheme.bodyMedium));

    return LayoutBuilder(builder: (context, constraint) {
      final widthImage = constraint.biggest.width;
      final point = (photoCardModel.height ?? 1) / (photoCardModel.width ?? 1);
      final heightImage = widthImage * point;
      return Card(
          margin: const EdgeInsets.symmetric(vertical: Constants.padding),
          child: ClipRRect(
              borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(borderRadius),
                  topRight: Radius.circular(borderRadius)),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    ImageWidget(
                        imageUrl: photoCardModel.imageUrl ?? "",
                        width: widthImage,
                        height: heightImage),
                    if (photoCardModel.namePhoto != null) namePhotoWidget,
                    authorWidget
                  ])));
    });
  }
}
