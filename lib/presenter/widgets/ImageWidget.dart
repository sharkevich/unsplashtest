import 'package:flutter/material.dart';
import 'package:unsplash_tes/presenter/Constants.dart';

class ImageWidget extends StatelessWidget {
  final String imageUrl;
  final double? width;
  final double? height;

  const ImageWidget(
      {super.key, required this.imageUrl, this.width, this.height});

  @override
  Widget build(BuildContext context) {
    Widget errorWidget = Center(
        child: Padding(
            padding: const EdgeInsets.all(Constants.padding / 2),
            child: Text(Constants.errorLoadingLabel,
                style: Theme.of(context).textTheme.labelMedium)));

    Widget loadingWidget = Center(
        child: Padding(
            padding: const EdgeInsets.all(Constants.padding / 2),
            child: Text(Constants.loadingLabel,
                style: Theme.of(context).textTheme.labelMedium)));

    return Image.network(imageUrl, width: width, height: height,
        errorBuilder: (context, error, stackTrace) {
      return errorWidget;
    }, loadingBuilder: (context, child, loadingProgress) {
      return loadingProgress == null ? child : loadingWidget;
    });
  }
}
