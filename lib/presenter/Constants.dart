class Constants {
  static const double padding = 16;

  static const String errorLoadingLabel = "Error loading!";
  static const String loadingLabel = "Loading...";
}