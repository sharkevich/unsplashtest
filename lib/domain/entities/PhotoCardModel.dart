import 'package:unsplash_tes/model/models/PhotoModel.dart';

class PhotoCardModel {
  String? id;
  String? imageUrl;
  String? namePhoto;
  String? authorName;
  int? width;
  int? height;

  PhotoCardModel(
      {this.id,
      this.imageUrl,
      this.namePhoto,
      this.authorName,
      this.width,
      this.height});
}

extension PhotoModelMapper on PhotoModel {
  PhotoCardModel toPhotoCardModel() {
    return PhotoCardModel(
        id: id,
        imageUrl: urls?.small,
        namePhoto: description,
        authorName: user?.name,
        width: width,
        height: height);
  }
}
