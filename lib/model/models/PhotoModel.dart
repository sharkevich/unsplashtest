import 'package:unsplash_tes/model/models/Links.dart';
import 'package:unsplash_tes/model/models/Urls.dart';
import 'package:unsplash_tes/model/models/User.dart';
import 'package:unsplash_tes/model/models/UserCollection.dart';

class PhotoModel {
  String? id;
  String? createdAt;
  String? updatedAt;
  int? width;
  int? height;
  String? color;
  String? blurHash;
  String? description;
  int? likes;
  User? user;
  List<UserCollection>? currentUserCollections;
  Urls? urls;
  Links? links;

  PhotoModel(
      {this.id,
      this.createdAt,
      this.updatedAt,
      this.width,
      this.height,
      this.color,
      this.blurHash,
      this.description,
      this.likes,
      this.user,
      this.currentUserCollections,
      this.urls,
      this.links});

  PhotoModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    width = json['width'];
    height = json['height'];
    color = json['color'];
    blurHash = json['blur_hash'];
    description = json['description'];
    urls = json['urls'] != null ? Urls.fromJson(json['urls']) : null;
    links = json['links'] != null ? Links.fromJson(json['links']) : null;
    likes = json['likes'];
    user = json['user'] != null ? User.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['width'] = width;
    data['height'] = height;
    data['color'] = color;
    data['blur_hash'] = blurHash;
    data['description'] = description;
    data['urls'] = urls != null ? urls!.toJson() : null;
    data['links'] = links != null ? links!.toJson() : null;
    data['likes'] = likes;
    data['user'] = user != null ? user!.toJson() : null;
    return data;
  }
}
