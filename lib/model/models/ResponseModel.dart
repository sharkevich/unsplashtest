class ResponseModel {
  String? data;
  int? statusCode;

  ResponseModel({this.data, this.statusCode});
}
