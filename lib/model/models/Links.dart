class Links {
  String? self;
  String? html;
  String? photos;
  String? likes;
  String? portfolio;
  String? download;
  String? downloadLocation;

  Links(
      {this.self,
      this.html,
      this.photos,
      this.likes,
      this.portfolio,
      this.download,
      this.downloadLocation});

  Links.fromJson(Map<String, dynamic> json) {
    self = json['self'];
    html = json['html'];
    photos = json['photos'];
    likes = json['likes'];
    portfolio = json['portfolio'];
    download = json['download'];
    downloadLocation = json['download_location'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['self'] = self;
    data['html'] = html;
    data['photos'] = photos;
    data['likes'] = likes;
    data['portfolio'] = portfolio;
    data['download'] = download;
    data['download_location'] = downloadLocation;
    return data;
  }
}
