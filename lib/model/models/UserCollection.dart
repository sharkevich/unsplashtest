import 'package:unsplash_tes/model/models/User.dart';

class UserCollection {
  int? id;
  String? title;
  String? publishedAt;
  String? lastCollectedAt;
  String? updatedAt;
  User? user;

  UserCollection(
      {this.id,
      this.title,
      this.publishedAt,
      this.lastCollectedAt,
      this.updatedAt,
      this.user});

  UserCollection.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    publishedAt = json['published_at'];
    lastCollectedAt = json['last_collected_at'];
    updatedAt = json['updated_at'];
    user = json['user'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['title'] = title;
    data['published_at'] = publishedAt;
    data['last_collected_at'] = lastCollectedAt;
    data['updated_at'] = updatedAt;
    data['user'] = user;
    return data;
  }
}
