import 'dart:convert';

import 'package:unsplash_tes/model/RestApiService.dart';
import 'package:unsplash_tes/model/models/ErrorModel.dart';
import 'package:unsplash_tes/model/models/PhotoModel.dart';

class Repository {
  static const String AUTHORITY = "api.unsplash.com";
  static const String PHOTOS = "photos";
  static const String CLIENT_ID =
      "ab3411e4ac868c2646c0ed488dfd919ef612b04c264f3374c97fff98ed253dc9";
  final unknownError = ErrorModel(errors: ["Error!"]);

  Future<List<PhotoModel>> getListPhotos() async {
    try {
      final responseModel = await RestApiService().sendRequest(
          authority: AUTHORITY,
          unencodedPath: PHOTOS,
          requestMethod: "GET",
          queryParameters: {"client_id": CLIENT_ID});
      if (responseModel.statusCode == 200) {
        return jsonDecode(responseModel.data ?? "")
            .map<PhotoModel>((e) => PhotoModel.fromJson(e))
            .toList();
      } else {
        throw ErrorModel.fromJson(jsonDecode(responseModel.data ?? ""));
      }
    } on ErrorModel catch (_) {
      rethrow;
    } catch (_) {
      throw unknownError;
    }
  }
}
