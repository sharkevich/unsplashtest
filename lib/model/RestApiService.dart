import 'dart:convert';

import 'package:http/http.dart';
import 'package:unsplash_tes/model/models/ResponseModel.dart';

class RestApiService {

  static const int successStatusCode = 200;

  Future<ResponseModel> sendRequest({
    required String authority,
    required String unencodedPath,
    required String requestMethod,
    Map<String, dynamic>? queryParameters}) async {

    final url = Uri.https(authority, unencodedPath, queryParameters);
    final request = Request(requestMethod, url);

    try {
      final response = await Client().send(request);
      final data = await response.stream.bytesToString(const Utf8Codec());
      final statusCode = response.statusCode;
      return ResponseModel(data: data, statusCode: statusCode);
    } catch (_) {
      throw Error();
    }
  }
}